package modules

import (
	"gitlab.com/Nachterretten/json-rpc/internal/infrastructure/component"
	acontroller "gitlab.com/Nachterretten/json-rpc/internal/modules/auth/controller"
	ucontroller "gitlab.com/Nachterretten/json-rpc/internal/modules/user/controller"
	wcontroller "gitlab.com/Nachterretten/json-rpc/internal/modules/worker/controllers"
)

type Controllers struct {
	Auth acontroller.Auther
	User ucontroller.Userer
	Work wcontroller.Workerer
}

func NewControllers(services *Services, components *component.Components) *Controllers {
	authController := acontroller.NewAuth(services.Auth, components)
	userController := ucontroller.NewUser(services.User, components)
	workController := wcontroller.NewWorker(services.Work, components)

	return &Controllers{
		Auth: authController,
		User: userController,
		Work: workController,
	}
}

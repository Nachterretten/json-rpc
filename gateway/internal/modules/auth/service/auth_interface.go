package service

import (
	"context"
)

type Auther interface {
	Register(ctx context.Context, in RegisterIn) RegisterOut
	AuthorizeEmail(ctx context.Context, in AuthorizeEmailIn) AuthorizeOut
	AuthorizeRefresh(ctx context.Context, in AuthorizeRefreshIn) AuthorizeOut
	AuthorizePhone(ctx context.Context, in AuthorizePhoneIn) AuthorizeOut
	SendPhoneCode(ctx context.Context, in SendPhoneCodeIn) SendPhoneCodeOut
	VerifyEmail(ctx context.Context, in VerifyEmailIn) VerifyEmailOut
}

const (
	RegisterEmail = iota + 1
	RegisterPhone
)

type VerifyEmailIn struct {
	Hash  string
	Email string
}

type VerifyEmailOut struct {
	Success   bool
	ErrorCode int
}

type SendPhoneCodeIn struct {
	Phone string
}

type SendPhoneCodeOut struct {
	Phone     string
	Code      int
	ErrorCode int
}

type AuthorizeIn struct {
	Email    string
	Password string
}

type AuthorizeOut struct {
	UserID       int
	AccessToken  string
	RefreshToken string
	ErrorCode    int
}

type RegisterIn struct {
	Email          string
	Phone          string
	Password       string
	IdempotencyKey string
}

type RegisterOut struct {
	Status    int
	ErrorCode int
}

type AuthorizeEmailIn struct {
	Email          string
	Password       string
	RetypePassword string
}

type AuthorizeRefreshIn struct {
	UserID int
}

type AuthorizePhoneIn struct {
	Phone string
	Code  int
}

type Out struct {
	ErrorCode int
}

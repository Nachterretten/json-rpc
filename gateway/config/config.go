package config

import (
	"go.uber.org/zap"
	"os"
	"strconv"
	"time"
)

const (
	AppName = "APP_NAME"

	serverPort         = "SERVER_PORT"
	envShutdownTimeout = "SHUTDOWN_TIMEOUT"
	envAccessTTL       = "ACCESS_TTL"
	envRefreshTTL      = "REFRESH_TTL"

	parseShutdownTimeoutError    = "config: parse server shutdown timeout error"
	parseRpcShutdownTimeoutError = "config: parse rpc server shutdown timeout error"
	parseTokenTTlError           = "config: parse token ttl error"
)

//go:generate easytags $GOFILE yaml, json

type AppConf struct {
	AppName     string    `yaml:"app_name" json:"app_name"`
	Environment string    `yaml:"environment" json:"environment"`
	Domain      string    `yaml:"domain" json:"domain"`
	APIUrl      string    `yaml:"api_url" json:"api_url"`
	Server      Server    `yaml:"server" json:"server"`
	Token       Token     `yaml:"token" json:"token"`
	Cors        Cors      `yaml:"cors" json:"cors"`
	Logger      Logger    `yaml:"logger" json:"logger"`
	RPCServer   RPCServer `yaml:"rpc_server" json:"rpc_server"`
	UserRPC     RPCClient `yaml:"client_rpc" json:"user_rpc"`
	AuthRPC     RPCClient `yaml:"client_rpc" json:"auth_rpc"`
	WorkerRPC   RPCClient `yaml:"client_rpc" json:"worker_rpc"`
}

type RPCClient struct {
	Host string `yaml:"host" json:"host"`
	Port string `yaml:"port" json:"port"`
}

type RPCServer struct {
	Port         string        `yaml:"port" json:"port"`
	ShutdownTime time.Duration `yaml:"shutdown_timeout" json:"shutdown_time"`
	Type         string        `yaml:"type" json:"type"`
}

type Logger struct {
	Level string `yaml:"level" json:"level"`
}

type Token struct {
	AccessTTL     time.Duration `yaml:"access_ttl" json:"access_ttl"`
	RefreshTTL    time.Duration `yaml:"refresh_ttl" json:"refresh_ttl"`
	AccessSecret  string        `yaml:"access_secret" json:"access_secret"`
	RefreshSecret string        `yaml:"refresh_secret" json:"refresh_secret"`
}

type Cors struct {
	// AllowedOrigins:   []string{"https://foo.com"}, // Use this to allow specific origin hosts
	AllowedOrigins   []string `yaml:"allowed_origins" json:"allowed_origins"`
	AllowedMethods   []string `yaml:"allowed_methods" json:"allowed_methods"`
	AllowedHeaders   []string `yaml:"allowed_headers" json:"allowed_headers"`
	ExposedHeaders   []string `yaml:"exposed_headers" json:"exposed_headers"`
	AllowCredentials bool     `yaml:"allow_credentials" json:"allow_credentials"`
	MaxAge           int      `yaml:"max_age" json:"max_age"` // Maximum value not ignored by any of major browsers
}

func newCors() *Cors {
	return &Cors{
		// AllowedOrigins:   []string{"https://foo.com"}, // Use this to allow specific origin hosts
		AllowedOrigins: []string{"https://*", "http://*"},
		// AllowOriginFunc:  func(r *http.Request, origin string) bool { return true },
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: false,
		MaxAge:           300, // Maximum value not ignored by any of major browsers
	}
}

func NewAppConfig() AppConf {
	port := os.Getenv(serverPort)
	return AppConf{
		AppName: os.Getenv(AppName),
		Server: Server{
			Port: port,
		},
		Cors: *newCors(),
	}
}

type Server struct {
	Port            string        `yaml:"port" json:"port"`
	ShutdownTimeout time.Duration `yaml:"shutdown_timeout" json:"shutdown_timeout"`
}

func (a *AppConf) Init(logger *zap.Logger) {
	shutDownTimeOut, err := strconv.Atoi(os.Getenv(envShutdownTimeout))
	if err != nil {
		logger.Fatal(parseShutdownTimeoutError)
	}
	shutDownTimeout := time.Duration(shutDownTimeOut) * time.Second
	if err != nil {
		logger.Fatal(parseRpcShutdownTimeoutError)
	}

	var accessTTL int
	accessTTL, err = strconv.Atoi(os.Getenv(envAccessTTL))
	if err != nil {
		logger.Fatal(parseTokenTTlError)
	}
	a.Token.AccessTTL = time.Duration(accessTTL) * time.Minute
	var refreshTTL int
	refreshTTL, err = strconv.Atoi(os.Getenv(envRefreshTTL))
	if err != nil {
		logger.Fatal(parseTokenTTlError)
	}

	a.RPCServer.Port = os.Getenv("RPC_PORT")
	a.UserRPC.Host = os.Getenv("USER_RPC_HOST")
	a.UserRPC.Port = os.Getenv("USER_RPC_PORT")
	a.AuthRPC.Host = os.Getenv("AUTH_RPC_HOST")
	a.AuthRPC.Port = os.Getenv("AUTH_RPC_PORT")
	a.WorkerRPC.Port = os.Getenv("WORKER_RPC_PORT")
	a.WorkerRPC.Host = os.Getenv("WORKER_RPC_HOST")
	a.RPCServer.Type = os.Getenv("RPC_TYPE") // выбираем jrpc/grpc

	a.Token.AccessSecret = os.Getenv("ACCESS_SECRET")
	a.Token.RefreshSecret = os.Getenv("REFRESH_SECRET")

	a.Server.ShutdownTimeout = shutDownTimeout

	a.Token.RefreshTTL = time.Duration(refreshTTL) * time.Hour * 24

}

package run

import (
	"context"
	"fmt"
	"github.com/go-chi/chi/v5"
	jsoniter "github.com/json-iterator/go"
	"github.com/ptflp/godecoder"
	"gitlab.com/Nachterretten/json-rpc/config"
	"gitlab.com/Nachterretten/json-rpc/internal/infrastructure/component"
	"gitlab.com/Nachterretten/json-rpc/internal/infrastructure/errors"
	"gitlab.com/Nachterretten/json-rpc/internal/infrastructure/responder"
	"gitlab.com/Nachterretten/json-rpc/internal/infrastructure/router"
	"gitlab.com/Nachterretten/json-rpc/internal/infrastructure/server"
	"gitlab.com/Nachterretten/json-rpc/internal/infrastructure/tools/cryptography"
	"gitlab.com/Nachterretten/json-rpc/internal/modules"
	"go.uber.org/zap"
	"golang.org/x/sync/errgroup"
	"net/http"

	"os"
)

// Application - интерфейс приложения
type Application interface {
	Runner
	Bootstraper
}

// Runner - интерфейс запуска приложения
type Runner interface {
	Run() int
}

// Bootstraper - интерфейс инициализации приложения
type Bootstraper interface {
	Bootstrap(options ...interface{}) Runner
}

// App - структура приложения
type App struct {
	conf     config.AppConf
	logger   *zap.Logger
	srv      server.Server
	jsonRPC  server.Server
	Sig      chan os.Signal
	services *modules.Services
}

// NewApp - конструктор приложения
func NewApp(conf config.AppConf, logger *zap.Logger) *App {
	return &App{conf: conf, logger: logger, Sig: make(chan os.Signal, 1)}
}

// Run - запуск приложения
func (a *App) Run() int {
	// на русском
	// создаем контекст для graceful shutdown
	ctx, cancel := context.WithCancel(context.Background())

	errGroup, ctx := errgroup.WithContext(ctx)

	// запускаем горутину для graceful shutdown
	// при получении сигнала SIGINT
	// вызываем cancel для контекста
	errGroup.Go(func() error {
		sigInt := <-a.Sig
		a.logger.Info("signal interrupt recieved", zap.Stringer("os_signal", sigInt))
		cancel()
		return nil
	})

	// запускаем http сервер
	errGroup.Go(func() error {
		err := a.srv.Serve(ctx)
		if err != nil && err != http.ErrServerClosed {
			a.logger.Error("app: server error", zap.Error(err))
			return err
		}
		return nil
	})

	if err := errGroup.Wait(); err != nil {
		return errors.GeneralError
	}

	return errors.NoError
}

// Bootstrap - инициализация приложения
func (a *App) Bootstrap(options ...interface{}) Runner {
	// инициализация менеджера токенов
	tokenManager := cryptography.NewTokenJWT(a.conf.Token)
	// инициализация декодера
	decoder := godecoder.NewDecoder(jsoniter.Config{
		EscapeHTML:             true,
		SortMapKeys:            true,
		ValidateJsonRawMessage: true,
		DisallowUnknownFields:  true,
	})
	// инициализация менеджера ответов сервера
	responseManager := responder.NewResponder(decoder, a.logger)
	// инициализация генератора uuid
	uuID := cryptography.NewUUIDGenerator()
	// инициализация хешера
	hash := cryptography.NewHash(uuID)
	// инициализация компонентов
	components := component.NewComponents(a.conf, tokenManager, responseManager, decoder, hash, a.logger)

	// инизализация сервиса в зависимости от выбора
	switch a.conf.RPCServer.Type {
	case "jrpc":
		services := modules.NewServicesJRPC(components)
		a.services = services
	case "grpc":
		services := modules.NewServiceGRPC(components)
		a.services = services
	}

	controllers := modules.NewControllers(a.services, components)
	// инициализация роутера
	var r *chi.Mux
	r = router.NewRouter(controllers, components)
	// конфигурация сервера
	srv := &http.Server{
		Addr:    fmt.Sprintf(":%s", a.conf.Server.Port),
		Handler: r,
	}

	// инициализация сервера
	a.srv = server.NewHttpServer(a.conf.Server, srv, a.logger)
	// возвращаем приложение
	return a
}

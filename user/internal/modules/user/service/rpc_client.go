package service

import (
	"context"
	"github.com/lib/pq"
	"gitlab.com/Nachterretten/json-rpc/internal/infrastructure/errors"
	"gitlab.com/Nachterretten/json-rpc/internal/infrastructure/tools/cryptography"
	"gitlab.com/Nachterretten/json-rpc/internal/modules/user/storage"
	"gitlab.com/Nachterretten/json-rpc/pkg/models"
	"go.uber.org/zap"
)

type UserService struct {
	storage storage.Userer
	logger  *zap.Logger
}

func NewUserService(storage storage.Userer, logger *zap.Logger) *UserService {
	u := &UserService{storage: storage, logger: logger}
	return u
}

func (u *UserService) Create(ctx context.Context, in UserCreateIn) UserCreateOut {
	var userOut models.UserDTO
	userOut.SetName(in.Name).
		SetPhone(in.Phone).
		SetEmail(in.Email).
		SetPassword(in.Password).
		SetEmailVerified(true).
		SetRole(in.Role)

	userID, err := u.storage.Create(ctx, userOut)
	if err != nil {
		if v, ok := err.(*pq.Error); ok && v.Code == "23505" {
			return UserCreateOut{
				ErrorCode: errors.UserServiceUserAlreadyExists,
			}
		}
		return UserCreateOut{
			ErrorCode: errors.UserServiceCreateUserErr,
		}
	}

	return UserCreateOut{
		UserID: userID,
	}
}

func (u *UserService) Update(ctx context.Context, in UserUpdateIn) UserUpdateOut {
	panic("implement me")
}

func (u *UserService) VerifyEmail(ctx context.Context, in UserVerifyEmailIn) UserUpdateOut {
	dto, err := u.storage.GetByID(ctx, in.UserID)
	if err != nil {
		u.logger.Error("jrpc: GetByEmail error", zap.Error(err))
		return UserUpdateOut{
			ErrorCode: errors.UserServiceRetrieveUsersErr,
		}
	}

	dto.SetEmailVerified(true)
	err = u.storage.Update(ctx, dto)
	if err != nil {
		u.logger.Error("jrpc: Update error", zap.Error(err))
		return UserUpdateOut{
			ErrorCode: errors.UserServiceUpdateErr,
		}
	}

	return UserUpdateOut{
		Success: true,
	}
}

func (u *UserService) ChangePassword(ctx context.Context, in ChangePasswordIn) ChangePasswordOut {
	dto, err := u.storage.GetByID(ctx, in.UserID)
	if !cryptography.CheckPassword(dto.GetPassword(), in.OldPassword) {
		return ChangePasswordOut{
			ErrorCode: errors.UserServiceChangePasswordErr,
		}
	}

	hashPass, err := cryptography.HashPassword(in.NewPassword)

	dto.SetPassword(hashPass)

	if err = u.storage.Update(ctx, dto); err != nil {
		u.logger.Error("parser: update error", zap.Error(err))
		return ChangePasswordOut{
			ErrorCode: errors.UserServiceChangePasswordErr,
		}
	}

	return ChangePasswordOut{
		Success: true,
	}
}

func (u *UserService) GetByEmail(ctx context.Context, in GetByEmailIn) UserOut {
	userDTO, err := u.storage.GetByEmail(ctx, in.Email)
	if err != nil {
		u.logger.Error("jrpc: GetByEmail error", zap.Error(err))
		return UserOut{
			ErrorCode: errors.UserServiceRetrieveUserErr,
		}
	}

	return UserOut{
		User: &models.User{
			ID:            userDTO.GetID(),
			Name:          userDTO.GetName(),
			Phone:         userDTO.GetPhone(),
			Email:         userDTO.GetEmail(),
			Password:      userDTO.GetPassword(),
			Role:          userDTO.GetRole(),
			Verified:      userDTO.Verified,
			EmailVerified: userDTO.EmailVerified,
			PhoneVerified: userDTO.PhoneVerified,
		},
	}
}

func (u *UserService) GetByPhone(ctx context.Context, in GetByPhoneIn) UserOut {
	panic("implement me")
}

func (u *UserService) GetByID(ctx context.Context, in GetByIDIn) UserOut {
	userDTO, err := u.storage.GetByID(ctx, in.UserID)
	if err != nil {
		u.logger.Error("jrpc: GetByID error", zap.Error(err))
		return UserOut{
			ErrorCode: errors.UserServiceRetrieveUserErr,
		}
	}

	return UserOut{
		User: &models.User{
			ID:            userDTO.GetID(),
			Name:          userDTO.GetName(),
			Phone:         userDTO.GetPhone(),
			Email:         userDTO.GetEmail(),
			Password:      userDTO.GetPassword(),
			Role:          userDTO.GetRole(),
			Verified:      userDTO.Verified,
			EmailVerified: userDTO.EmailVerified,
			PhoneVerified: userDTO.PhoneVerified,
		},
	}
}

func (u *UserService) GetByIDs(ctx context.Context, in GetByIDsIn) UsersOut {
	panic("implement me")
}

package grpc

import (
	"context"
	"gitlab.com/Nachterretten/json-rpc/internal/modules/user/service"
	"gitlab.com/Nachterretten/json-rpc/pkg/models"
	"gitlab.com/Nachterretten/json-rpc/rpc/grpc/proto_user"
)

type UserServiceGRPC struct {
	user service.Userer
}

func NewUserServiceGRPC(user service.Userer) *UserServiceGRPC {
	return &UserServiceGRPC{user: user}
}

func (u *UserServiceGRPC) Create(ctx context.Context, in *proto_user.UserCreateIn) (*proto_user.UserCreateOut, error) {
	out := u.user.Create(ctx, service.UserCreateIn{
		Name:     in.GetName(),
		Phone:    in.GetPhone(),
		Email:    in.GetEmail(),
		Password: in.GetPassword(),
		Role:     int(in.GetRole()),
	})
	return &proto_user.UserCreateOut{
		UserId:    int32(out.UserID),
		ErrorCode: int32(out.ErrorCode),
	}, nil
}

func (u *UserServiceGRPC) Update(ctx context.Context, in *proto_user.UserUpdateIn) (*proto_user.UserUpdateOut, error) {
	out := u.user.Update(ctx, service.UserUpdateIn{
		User: models.User{
			ID:            int(in.GetUser().Id),
			Name:          in.GetUser().Name,
			Phone:         in.GetUser().Phone,
			Email:         in.GetUser().Email,
			Password:      in.GetUser().Password,
			Role:          int(in.GetUser().Role),
			Verified:      in.GetUser().Verified,
			EmailVerified: in.GetUser().EmailVerified,
			PhoneVerified: in.GetUser().PhoneVerified,
		},
		Fields: nil,
	})
	return &proto_user.UserUpdateOut{
		Success:   out.Success,
		ErrorCode: int32(out.ErrorCode),
	}, nil
}

func (u *UserServiceGRPC) VerifyEmail(ctx context.Context, in *proto_user.UserVerifyEmailIn) (*proto_user.UserUpdateOut, error) {
	out := u.user.VerifyEmail(ctx, service.UserVerifyEmailIn{
		UserID: int(in.GetUserId())})

	return &proto_user.UserUpdateOut{
		Success:   out.Success,
		ErrorCode: int32(out.ErrorCode),
	}, nil
}

func (u *UserServiceGRPC) ChangePassword(ctx context.Context, in *proto_user.ChangePasswordIn) (*proto_user.ChangePasswordOut, error) {
	out := u.user.ChangePassword(ctx, service.ChangePasswordIn{
		UserID:      int(in.GetUserId()),
		OldPassword: in.GetOldPassword(),
		NewPassword: in.GetNewPassword(),
	})
	return &proto_user.ChangePasswordOut{
		Success:   out.Success,
		ErrorCode: int32(out.ErrorCode),
	}, nil
}

func (u *UserServiceGRPC) GetByEmail(ctx context.Context, in *proto_user.GetByEmailIn) (*proto_user.UserOut, error) {
	out := u.user.GetByEmail(ctx, service.GetByEmailIn{
		Email: in.GetEmail()})
	return &proto_user.UserOut{
		User: &proto_user.User{
			Id:            int32(out.User.ID),
			Name:          out.User.Name,
			Phone:         out.User.Phone,
			Email:         out.User.Email,
			Password:      out.User.Password,
			Role:          int32(out.User.Role),
			Verified:      out.User.Verified,
			EmailVerified: out.User.EmailVerified,
			PhoneVerified: out.User.PhoneVerified,
		},
		ErrorCode: int32(out.ErrorCode),
	}, nil
}

func (u *UserServiceGRPC) GetByPhone(ctx context.Context, in *proto_user.GetByPhoneIn) (*proto_user.UserOut, error) {
	out := u.user.GetByPhone(ctx, service.GetByPhoneIn{
		Phone: in.GetPhone()})
	return &proto_user.UserOut{
		User: &proto_user.User{
			Id:            int32(out.User.ID),
			Name:          out.User.Name,
			Phone:         out.User.Phone,
			Email:         out.User.Email,
			Password:      out.User.Password,
			Role:          int32(out.User.Role),
			Verified:      out.User.Verified,
			EmailVerified: out.User.EmailVerified,
			PhoneVerified: out.User.PhoneVerified,
		},
		ErrorCode: int32(out.ErrorCode),
	}, nil
}

func (u *UserServiceGRPC) GetByID(ctx context.Context, in *proto_user.GetByIDIn) (*proto_user.UserOut, error) {
	out := u.user.GetByID(ctx, service.GetByIDIn{
		UserID: int(in.GetUserId())})
	return &proto_user.UserOut{
		User: &proto_user.User{
			Id:            int32(out.User.ID),
			Name:          out.User.Name,
			Phone:         out.User.Phone,
			Email:         out.User.Email,
			Password:      out.User.Password,
			Role:          int32(out.User.Role),
			Verified:      out.User.Verified,
			EmailVerified: out.User.EmailVerified,
			PhoneVerified: out.User.PhoneVerified,
		},
		ErrorCode: int32(out.ErrorCode),
	}, nil
}

func (u *UserServiceGRPC) GetByIDs(ctx context.Context, in *proto_user.GetByIDsIn) (*proto_user.UsersOut, error) {
	users := make([]*proto_user.User, len(in.GetUserIds()))
	sl32 := make([]int, len(in.GetUserIds()))
	for i := range in.GetUserIds() {
		sl32[i] = int(in.GetUserIds()[i])
	}
	out := u.user.GetByIDs(ctx, service.GetByIDsIn{UserIDs: sl32})

	for _, user := range out.User {
		users = append(users, &proto_user.User{
			Id:            int32(user.ID),
			Name:          user.Name,
			Phone:         user.Phone,
			Email:         user.Email,
			Password:      user.Password,
			Role:          int32(user.Role),
			Verified:      user.Verified,
			EmailVerified: user.EmailVerified,
			PhoneVerified: user.PhoneVerified,
		})
	}
	return &proto_user.UsersOut{
		User:      users,
		ErrorCode: int32(out.ErrorCode),
	}, nil
}

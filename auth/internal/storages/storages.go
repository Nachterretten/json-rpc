package storages

import (
	"gitlab.com/Nachterretten/json-rpc/internal/db/adapter"
	"gitlab.com/Nachterretten/json-rpc/internal/infrastructure/cache"
	vstorage "gitlab.com/Nachterretten/json-rpc/internal/modules/auth/storage"
)

type Storages struct {
	//User   ustorage.Userer
	Verify vstorage.Verifier
}

func NewStorages(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *Storages {
	return &Storages{
		//User:   ustorage.NewUserStorage(sqlAdapter, cache),
		Verify: vstorage.NewEmailVerify(sqlAdapter),
	}
}

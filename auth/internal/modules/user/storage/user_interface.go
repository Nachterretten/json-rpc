package storage

import (
	"context"
	"gitlab.com/Nachterretten/json-rpc/pkg/models"
)

type Userer interface {
	Create(ctx context.Context, u models.UserDTO) (int, error)
	Update(ctx context.Context, u models.UserDTO) error
	GetByID(ctx context.Context, userID int) (models.UserDTO, error)
	GetByIDs(ctx context.Context) ([]models.UserDTO, error)
	GetByEmail(ctx context.Context, email string) (models.UserDTO, error)
	GetByFilter(ctx context.Context) ([]models.UserDTO, error)
}

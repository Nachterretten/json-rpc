package service

import (
	"context"
	"gitlab.com/Nachterretten/json-rpc/pkg/models"
	"gitlab.com/Nachterretten/json-rpc/rpc/gRPC/proto_user"

	"gitlab.com/Nachterretten/json-rpc/internal/infrastructure/errors"
)

type ExchangeServiceGRPCClient struct {
	client proto_user.UserServiceRPCClient
}

func NewExchangeServiceGRPC(client proto_user.UserServiceRPCClient) *ExchangeServiceGRPCClient {
	return &ExchangeServiceGRPCClient{client: client}
}

func (u *ExchangeServiceGRPCClient) Create(ctx context.Context, in UserCreateIn) UserCreateOut {
	req, err := u.client.Create(ctx, &proto_user.UserCreateIn{
		Name:     in.Name,
		Phone:    in.Phone,
		Email:    in.Email,
		Password: in.Password,
		Role:     int32(in.Role),
	})
	if err != nil {
		return UserCreateOut{
			UserID:    0,
			ErrorCode: errors.UserServiceCreateUserErr,
		}
	}
	return UserCreateOut{
		UserID:    int(req.GetUserId()),
		ErrorCode: int(req.GetErrorCode()),
	}
}

func (u *ExchangeServiceGRPCClient) Update(ctx context.Context, in UserUpdateIn) UserUpdateOut {
	sl32 := make([]int32, len(in.Fields))
	for i := range in.Fields {
		sl32[i] = int32(in.Fields[i])
	}
	req, err := u.client.Update(ctx, &proto_user.UserUpdateIn{
		User: &proto_user.User{
			Id:            int32(in.User.ID),
			Name:          in.User.Name,
			Phone:         in.User.Phone,
			Email:         in.User.Email,
			Password:      in.User.Password,
			Role:          int32(in.User.Role),
			Verified:      in.User.Verified,
			EmailVerified: in.User.EmailVerified,
			PhoneVerified: in.User.PhoneVerified,
		},
		Fields: sl32,
	})
	if err != nil {
		return UserUpdateOut{
			Success:   false,
			ErrorCode: errors.UserServiceUpdateErr,
		}
	}
	return UserUpdateOut{
		Success: req.GetSuccess(),
	}
}

func (u *ExchangeServiceGRPCClient) VerifyEmail(ctx context.Context, in UserVerifyEmailIn) UserUpdateOut {
	req, err := u.client.VerifyEmail(ctx, &proto_user.UserVerifyEmailIn{
		UserId: int32(in.UserID),
	})
	if err != nil {
		return UserUpdateOut{
			ErrorCode: errors.AuthServiceVerifyErr,
		}
	}
	return UserUpdateOut{
		Success:   req.GetSuccess(),
		ErrorCode: int(req.GetErrorCode()),
	}
}

func (u *ExchangeServiceGRPCClient) ChangePassword(ctx context.Context, in ChangePasswordIn) ChangePasswordOut {
	req, err := u.client.ChangePassword(ctx, &proto_user.ChangePasswordIn{
		UserId:      int32(in.UserID),
		OldPassword: in.OldPassword,
		NewPassword: in.NewPassword,
	})
	if err != nil {
		return ChangePasswordOut{
			ErrorCode: errors.UserServiceChangePasswordErr,
		}
	}
	return ChangePasswordOut{
		Success:   req.GetSuccess(),
		ErrorCode: int(req.GetErrorCode()),
	}
}

func (u *ExchangeServiceGRPCClient) GetByEmail(ctx context.Context, in GetByEmailIn) UserOut {
	req, err := u.client.GetByEmail(ctx, &proto_user.GetByEmailIn{
		Email: in.Email,
	})
	if err != nil {
		return UserOut{
			ErrorCode: errors.UserServiceRetrieveUsersErr,
		}
	}
	return UserOut{
		User: &models.User{
			ID:            int(req.GetUser().Id),
			Name:          req.GetUser().Name,
			Phone:         req.GetUser().Phone,
			Email:         req.GetUser().Email,
			Password:      req.GetUser().Password,
			Role:          int(req.GetUser().Role),
			Verified:      req.GetUser().Verified,
			EmailVerified: req.GetUser().EmailVerified,
			PhoneVerified: req.GetUser().PhoneVerified,
		},
	}
}

func (u *ExchangeServiceGRPCClient) GetByPhone(ctx context.Context, in GetByPhoneIn) UserOut {
	req, err := u.client.GetByPhone(ctx, &proto_user.GetByPhoneIn{
		Phone: in.Phone,
	})
	if err != nil {
		return UserOut{
			ErrorCode: errors.UserServiceRetrieveUsersErr,
		}
	}
	return UserOut{
		User: &models.User{
			ID:            int(req.GetUser().Id),
			Name:          req.GetUser().Name,
			Phone:         req.GetUser().Phone,
			Email:         req.GetUser().Email,
			Password:      req.GetUser().Password,
			Role:          int(req.GetUser().Role),
			Verified:      req.GetUser().Verified,
			EmailVerified: req.GetUser().EmailVerified,
			PhoneVerified: req.GetUser().PhoneVerified,
		},
	}
}

func (u *ExchangeServiceGRPCClient) GetByID(ctx context.Context, in GetByIDIn) UserOut {
	req, err := u.client.GetByID(ctx, &proto_user.GetByIDIn{
		UserId: int32(in.UserID),
	})
	if err != nil {
		return UserOut{
			ErrorCode: errors.UserServiceRetrieveUsersErr,
		}
	}
	return UserOut{
		User: &models.User{
			ID:            int(req.GetUser().Id),
			Name:          req.GetUser().Name,
			Phone:         req.GetUser().Phone,
			Email:         req.GetUser().Email,
			Password:      req.GetUser().Password,
			Role:          int(req.GetUser().Role),
			Verified:      req.GetUser().Verified,
			EmailVerified: req.GetUser().EmailVerified,
			PhoneVerified: req.GetUser().PhoneVerified,
		},
	}
}

func (u *ExchangeServiceGRPCClient) GetByIDs(ctx context.Context, in GetByIDsIn) UsersOut {
	users := make([]models.User, len(in.UserIDs))
	sl32 := make([]int32, len(in.UserIDs))
	for i := range in.UserIDs {
		sl32[i] = int32(in.UserIDs[i])
	}
	req, err := u.client.GetByIDs(ctx, &proto_user.GetByIDsIn{UserIds: sl32})
	if err != nil {
		return UsersOut{
			ErrorCode: errors.UserServiceRetrieveUsersErr,
		}
	}
	for _, user := range req.GetUser() {
		users = append(users, models.User{
			ID:            int(user.Id),
			Name:          user.Name,
			Phone:         user.Phone,
			Email:         user.Email,
			Password:      user.Password,
			Role:          int(user.Role),
			Verified:      user.Verified,
			EmailVerified: user.EmailVerified,
			PhoneVerified: user.PhoneVerified,
		})
	}
	return UsersOut{
		User: users,
	}
}
